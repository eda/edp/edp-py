from . import SweepAnalysis
from . import Instance
from . import Net

class NoiseFrequencySweepAnalysis(SweepAnalysis.SweepAnalysis):
    """Handle to a noise frequency sweep analysis in the test"""

    def __init__(self, javaObj):
        """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
        super().__init__(javaObj)

    def getPlus(self):
        import edlab.eda.edp.simulation.datastruct.Net as JNet
        javaRetval = self._javaObj.getPlus()
        if JNet.isInstanceOf(javaRetval):
            return Net.Net.init(javaRetval)
        else:
            return None

    def setPlus(self, net: Net.Net):
        import edlab.eda.edp.simulation.datastruct.Net as JNet
        javaRetval = self._javaObj.setPlus(net._javaObj)
        return JNet.isInstanceOf(javaRetval)

    def getMinus(self):
        import edlab.eda.edp.simulation.datastruct.Net as JNet
        javaRetval = self._javaObj.geMinus()
        if JNet.isInstanceOf(javaRetval):
            return Net.Net.init(javaRetval)
        else:
            return None

    def setMinus(self, net: Net.Net):
        import edlab.eda.edp.simulation.datastruct.Net as JNet
        javaRetval = self._javaObj.setMinus(net._javaObj)
        return JNet.isInstanceOf(javaRetval)

    def getInputProbe(self):
        import edlab.eda.edp.simulation.datastruct.Instance as JInstance
        javaRetval = self._javaObj.getInputProbe()
        if JInstance.isInstanceOf(javaRetval):
            return Instance.Instance.init(javaRetval)
        else:
            return None

    def setInputProbe(self, probe: Instance.Instance):
        import edlab.eda.edp.simulation.datastruct.Instance as JInstance
        javaRetval = self._javaObj.setInputProbe(probe._javaObj)
        return JInstance.isInstanceOf(javaRetval)
            
    def getOutputProbe(self):
        import edlab.eda.edp.simulation.datastruct.Instance as JInstance
        javaRetval = self._javaObj.getInputProbe()
        if JInstance.isOutputProbe(javaRetval):
            return Instance.Instance.init(javaRetval)
        else:
            return None

    def setOutputProbe(self, probe: Instance.Instance):
        import edlab.eda.edp.simulation.datastruct.Instance as JInstance
        javaRetval = self._javaObj.setOutputProbe(probe._javaObj)
        return JInstance.isInstanceOf(javaRetval)

    @staticmethod
    def init(javaObj):
        import edlab.eda.edp.simulation.analysis.NoiseFrequencySweepAnalysis as JNoiseFrequencySweepAnalysis

        if JNoiseFrequencySweepAnalysis.isInstanceOf(javaObj):
            return NoiseFrequencySweepAnalysis(javaObj)
