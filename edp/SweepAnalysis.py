# EDP imports
from . import Analysis

class SweepAnalysis(Analysis.Analysis):
  """Handle to a Analysis in a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getValues(self) -> list[float]:
    """Get the values in the sweep"""
    javaArray = self._javaObj.getValues()

    retval = []

    for elem in javaArray:
      retval.append(float(elem.doubleValue()))
    
    return retval
    
  def setValues(self, values:list[float]) -> list[float]:
    """Set the values in the sweep"""
    
    javaArray = []
    
    import java.math.BigDecimal as JBigDecimal
    
    for elem in values:
      javaArray.append(JBigDecimal(elem))   

    javaArray = self._javaObj.setValues(javaArray)
    retval = []
    
    for elem in javaArray:
      retval.append(float(elem.doubleValue()))
    
    return retval
