# jpype imports
import jpype

from .ExpertDesignPlanWrapper import *
from . import Database

class ReferenceSchematicSymbolGenerator(ExpertDesignPlanWrapper):
  """Generator for a symbol based on a given schematic in the Expert Design Plan Toolbox"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def show(self, show:bool) -> bool:
    """Show the result of the generator in the Virtuoso Symbol editor
    
    Parameters
    ----------
    show : bool
      Show generator result
    """    
    return self._javaObj.show(show)
    
  def execute(self) -> bool:
    '''
    Execute the generator
    '''
    return self._javaObj.execute()
    
  def getScale(self) -> float:
    '''
    Get the scaling of the symbol
    '''
    return self._javaObj.getScale().doubleValue() 
    
  def getScale(self,scale:float) -> float:
    '''
    Set the scaling of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setScale(JBigDecimal(scale)).doubleValue() 
    
  def getStubLength(self) -> float:
    '''
    Get the sub length of the symbol
    '''
    return self._javaObj.getStubLength().doubleValue() 
    
  def setStubLength(self,value:float) -> float:
    '''
    Set the sub length of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setStubLength(JBigDecimal(value)).doubleValue() 
    
  def getLeftMargin(self) -> float:
    '''
    Get the left margin of the symbol
    '''
    return self._javaObj.getLeftMargin().doubleValue()  
    
  def setLeftMargin(self,value:float) -> float:
    '''
    Set the left margin of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setLeftMargin(JBigDecimal(value)).doubleValue() 
    
  def getTopMargin(self) -> float:
    '''
    Get the top margin of the symbol
    '''
    return self._javaObj.getTopMargin().doubleValue()  
    
  def setTopMargin(self,value:float) -> float:
    '''
    Set the top margin of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setTopMargin(JBigDecimal(value)).doubleValue() 
    
  def getRightMargin(self) -> float:
    '''
    Get the right margin of the symbol
    '''
    return self._javaObj.getRightMargin().doubleValue()  
    
  def setRightMargin(self,value:float) -> float:
    '''
    Set the right margin of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setRightMargin(JBigDecimal(value)).doubleValue() 
    
  def getBottomMargin(self) -> float:
    '''
    Get the bottom margin of the symbol
    '''
    return self._javaObj.getBottomMargin().doubleValue()   
    
  def setBottomMargin(self,value:float) -> float:
    '''
    Set the bottom margin of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setBottomMargin(JBigDecimal(value)).doubleValue() 
    
  def getFontHeight(self) -> float:
    '''
    Get the font height
    '''
    return self._javaObj.getFontHeight().doubleValue()   
    
  def setFontHeight(self,value:float) -> float:
    '''
    Set the font height of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setFontHeight(JBigDecimal(value)).doubleValue() 
    
  def getLabelOffset(self) -> float:
    '''
    Get the label offset
    '''
    return self._javaObj.getLabelOffset().doubleValue()   
    
  def setLabelOffset(self,value:float) -> float:
    '''
    Set the label offset of the symbol
    '''
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setLabelOffset(JBigDecimal(value)).doubleValue() 
    
     
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.symgen.rect.schematic.ReferenceSchematicSymbolGenerator as JReferenceSchematicSymbolGenerator

    if JReferenceSchematicSymbolGenerator.isInstanceOf(javaObj) :
      return ReferenceSchematicSymbolGenerator(javaObj)
