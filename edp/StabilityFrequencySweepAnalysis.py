from . import SweepAnalysis
from . import Instance
from . import Net

class StabilityFrequencySweepAnalysis(SweepAnalysis.SweepAnalysis):
    """Handle to a noise frequency sweep analysis in the test"""

    def __init__(self, javaObj):
        """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
        super().__init__(javaObj)

    def getGround(self):
        import edlab.eda.edp.simulation.datastruct.Net as JNet
        javaRetval = self._javaObj.geGround()
        if JNet.isInstanceOf(javaRetval):
            return Net.Net.init(javaRetval)
        else:
            return None

    def setGround(self, net: Net.Net):
        import edlab.eda.edp.simulation.datastruct.Net as JNet
        javaRetval = self._javaObj.setGround(net._javaObj)
        return JNet.isInstanceOf(javaRetval)

    def getProbe(self):
        import edlab.eda.edp.simulation.datastruct.Instance as JInstance
        javaRetval = self._javaObj.getProbe()
        if JInstance.isInstanceOf(javaRetval):
            return Instance.Instance.init(javaRetval)
        else:
            return None

    def setProbe(self, probe: Instance.Instance):
        import edlab.eda.edp.simulation.datastruct.Instance as JInstance
        javaRetval = self._javaObj.setProbe(probe._javaObj)
        return JInstance.isInstanceOf(javaRetval)
            
    @staticmethod
    def init(javaObj):
        import edlab.eda.edp.simulation.analysis.StabilityFrequencySweepAnalysis as JStabilityFrequencySweepAnalysis

        if JStabilityFrequencySweepAnalysis.isInstanceOf(javaObj):
            return StabilityFrequencySweepAnalysis(javaObj)
