# EDP imports
from . import Analysis
from . import DatabaseParameter

class AlterAnalysis(Analysis.Analysis):
  """Handle to an alter analysis in the test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def setParameter(self, parameter:DatabaseParameter) -> DatabaseParameter:
    """Set the parameter to be altered"""
    return DatabaseParameter.DatabaseParameter.init(self._javaObj.setParameter(parameter._javaObj))

  def getParameter(self) -> DatabaseParameter:
    """Get the parameter that is altered"""
    return DatabaseParameter.DatabaseParameter.init(self._javaObj.getParameter())
    
  def getValue(self):
    """Get the value of the parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue=self._javaObj.getValue()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def setValue(self, value:[float,int, bool,str]) ->[float,int, bool,str]:
    """Set the value of the parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.setValue(value)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue

    
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.analysis.AlterAnalysis as JAlterAnalysis

    if JAlterAnalysis.isInstanceOf(javaObj) :
      return AlterAnalysis(javaObj)
