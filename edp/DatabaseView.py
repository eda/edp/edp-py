from . import ExpertDesignPlanWrapper
from . import Database
from . import DatabaseCell

class DatabaseView(ExpertDesignPlanWrapper):
  """Reference of a View in the Expert Design Plan"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getLibraryName(self) -> str:
    '''
    Get the library name of the view
    '''
    return str(self._javaObj.getLibraryName())

  def getCellName(self) -> str:
    '''
    Get the cell name of the view
    '''
    return str(self._javaObj.getCellName())
    
  def getViewName(self) -> str:
    '''
    Get the view name of the view
    '''
    return str(self._javaObj.getViewName())

  def getDatabase(self)  -> Database:
    """Get the database where the cell is stored"""
    
    return Database.Database._initdb(self._javaObj.getDatabase())
    
  def getCell(self)  -> DatabaseCell:
    """Get the cell where the view is stored"""
    
    return DatabaseCell.DatabaseCell.init(self._javaObj.getCell())
    
  def show(self)  -> bool:
    """Show the view"""
    
    return self._javaObj.show()
    
  def getNets(self) -> list[str]:
    '''
    Get all nets in the view as list
    '''
    javaArray = self._javaObj.getNetsAsArray()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval  
