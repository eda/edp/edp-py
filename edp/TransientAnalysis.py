# EDP imports
from . import Analysis
from . import DatabaseParameter
from jpype.types import JInt

class TransientAnalysis(Analysis.Analysis):
  """Handle to an transient analysis in the test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)

  def getStopTime(self):
    """Get the stop time of the analysis"""
    return self._javaObj.getStopTime().doubleValue()
    
  def setStopTime(self, stopTime:float):
    """Set the stop time of the analysis"""
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setStopTime(JBigDecimal(stopTime)).doubleValue()  

  def getCmin(self):
    return self._javaObj.getCmin().doubleValue()
    
  def setCmin(self, cmin:float):
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setCmin(JBigDecimal(cmin)).doubleValue()  

  def getMethod(self):
    return self._javaObj.getMethod()
    
  def setMethod(self, method:str):
    return self._javaObj.setMethod(method)

  def getStrobeperiod(self):
    return self._javaObj.getStrobeperiod().doubleValue()
    
  def setStrobeperiod(self, strobeperiod:float):
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setStrobeperiod(JBigDecimal(strobeperiod)).doubleValue()  

  def getStrobedelay(self):
    return self._javaObj.getStrobedelay().doubleValue()
    
  def setStrobedelay(self, strobedelay:float):
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setStrobedelay(JBigDecimal(strobedelay)).doubleValue()  

  def getNoisefmin(self):
    noisefmin = self._javaObj.getNoisefmin()
    return noisefmin.doubleValue() if noisefmin else None 
    
  def setNoisefmin(self, noisefmin:float):
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setNoisefmin(JBigDecimal(noisefmin)).doubleValue()  

  def getNoisefmax(self):
    noisefmax = self._javaObj.getNoisefmax()
    return noisefmax.doubleValue() if noisefmax else None
    
  def setNoisefmax(self, noisefmax:float):
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setNoisefmax(JBigDecimal(noisefmax)).doubleValue()  

  def getNoiseseed(self):
    noiseseed = self._javaObj.getNoiseseed()
    return int(noiseseed) if noiseseed else None
    
  def setNoiseseed(self, noiseseed:int):
    return int(self._javaObj.setNoiseseed(noiseseed))

  def getNoisescale(self):
    noisescale = self._javaObj.getNoisescale()
    return noisescale.doubleValue() if noisescale else None
    
  def setNoisescale(self, noisescale:float):
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setNoisescale(JBigDecimal(noisescale)) # .doubleValue()  

  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.analysis.TransientAnalysis as JTransientAnalysis

    if JTransientAnalysis.isInstanceOf(javaObj) :
      return TransientAnalysis(javaObj)
