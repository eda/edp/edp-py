from .ExpertDesignPlanWrapper import *
from . import DatabaseInstance
from . import DatabaseParameter

class MatchingParametersGroup(ExpertDesignPlanWrapper):
  """Reference of a Matching Group in the Expert Design Plan"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def addParameter(self,parameter:DatabaseParameter) -> bool:
    '''
    Add a parameter to the matching group
    '''
    return self._javaObj.addParameter(parameter._javaObj)
    
  def removeParameter(self, parameter:DatabaseParameter) -> bool:
    '''
    Remove a parameter from the matching group
    '''
    return self._javaObj.removeParameter(parameter._javaObj)
    
  def clear(self) -> bool:
    '''
    Clear matching group
    '''
    return self._javaObj.clear()
    
  def getParameters(self) -> list[DatabaseParameter]:
    '''
    Get parameters as list
    '''
    
    javaArray = self._javaObj.getParameters()

    retval = []

    for elem in javaArray:
      retval.append(DatabaseParameter.DatabaseParameter.init(elem))

    return retval
    
  def getValue(self):
    """Get the value of the matching group"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.get()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def set(self, value:[float,int, bool,str]) ->[float,int, bool,str]:
    """Set the value of the matching group"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.set(value)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.MatchingParametersGroup as JMatchingParametersGroup

    if JMatchingParametersGroup.isInstanceOf(javaObj) :
      return MatchingParametersGroup(javaObj)
