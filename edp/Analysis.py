# EDP imports
from .ExpertDesignPlanWrapper import *
from . import Test

class Analysis(ExpertDesignPlanWrapper):
  """Handle to a Analysis in a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getTitle(self):
    """Get the title of the analysis"""
    return str(self._javaObj.getTitle())
    
  def getAnalysisIdentifier(self):
    """Get the identifier of the analysis"""
    return str(self._javaObj.getAnalysisIdentifier())
    
  def getTest(self):
    """Get the test where the analysis is defined"""
    return Test.Test.init(self._javaObj.getTest())

  @staticmethod
  def buildAnalysis(javaObj):

    from . import OperatingPointAnalysis
    from . import DCParameterSweepAnalysis
    from . import AlterAnalysis    
    from . import TransientAnalysis    
    from . import StabilityFrequencySweepAnalysis    
    from . import NoiseFrequencySweepAnalysis    
        
    import edlab.eda.edp.simulation.analysis.OperatingPointAnalysis as JOperatingPointAnalysis
    import edlab.eda.edp.simulation.analysis.DCParameterSweepAnalysis as JDCParameterSweepAnalysis
    import edlab.eda.edp.simulation.analysis.AlterAnalysis as JAlterAnalysis
    import edlab.eda.edp.simulation.analysis.TransientAnalysis as JTransientAnalysis
    import edlab.eda.edp.simulation.analysis.StabilityFrequencySweepAnalysis as JStabilityFrequencySweepAnalysis
    import edlab.eda.edp.simulation.analysis.NoiseFrequencySweepAnalysis as JNoiseFrequencySweepAnalysis
           
    if JOperatingPointAnalysis.isInstanceOf(javaObj) :
      return OperatingPointAnalysis.OperatingPointAnalysis.init(javaObj)
    elif JDCParameterSweepAnalysis.isInstanceOf(javaObj) :
      return DCParameterSweepAnalysis.DCParameterSweepAnalysis.init(javaObj)
    elif JAlterAnalysis.isInstanceOf(javaObj) :
      return AlterAnalysis.AlterAnalysis.init(javaObj)
    elif JTransientAnalysis.isInstanceOf(javaObj) :
      return TransientAnalysis.TransientAnalysis.init(javaObj)    
    elif JStabilityFrequencySweepAnalysis.isInstanceOf(javaObj) :
      return StabilityFrequencySweepAnalysis.StabilityFrequencySweepAnalysis.init(javaObj)    
    elif JNoiseFrequencySweepAnalysis.isInstanceOf(javaObj) :
      return NoiseFrequencySweepAnalysis.NoiseFrequencySweepAnalysis.init(javaObj)    
    else:
      return None

