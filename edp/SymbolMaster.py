from .ExpertDesignPlanWrapper import *
from . import Database

class SymbolMaster(ExpertDesignPlanWrapper):
  """Handle to a an existing symbol in Cadence Virtuoso. It is used to create instances."""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getLibraryName(self) -> str:
    '''
    Get the library name of the master
    '''
    return str(self._javaObj.getLibraryName())

  def getCellName(self) -> str:
    '''
    Get the cell name of the master
    '''
    return str(self._javaObj.getCellName())
    
  def getViewName(self) -> str:
    '''
    Get the view name of the master
    '''
    return str(self._javaObj.getViewName())
    
  def getBoundingBox(self) -> list[list[float]]:
    '''
    Get the bounding box of the master
    '''
    
    bbox = self._javaObj.getBoundingBox()
    
    return [[bbox.getLowerLeft().getX().doubleValue(), 
      bbox.getLowerLeft().getY().doubleValue()],
      [bbox.getUpperRight().getX().doubleValue(), 
      bbox.getUpperRight().getY().doubleValue()]]
    
  def getCenter(self) -> list[float]:
    '''
    Get the center of the master
    '''
    
    point = self._javaObj.getCenter()
    
    return [point.getX().doubleValue(), point.getY().doubleValue()]
    
  def getTerminals(self) -> list[str]:
    """Get the terminals of the master"""
    javaArray = self._javaObj.getTerminals()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def isTerminal(self, name:str) -> bool:
    """Check if a terminal with a given name is available"""
    return self._javaObj.isTerminal(name)
    
  def getParameters(self) -> list[str]:
    """Get the parameters of the master"""
    javaArray = self._javaObj.getParameters()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def isParameter(self, name:str) -> bool:
    """Check if a parameter with a given name is available"""
    return self._javaObj.isParameter(name)
    
  def getPosition(self, name:str) -> list[float]:
    """Get the position of a dedicated terminal"""
    point = self._javaObj.getPosition(name)  
    
    if point is None:
      return []
    else:
      return [point.getX().doubleValue(), point.getY().doubleValue()]
  
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.schgen.SymbolMaster as JSymbolMaster

    if JSymbolMaster.isInstanceOf(javaObj) :
      return SymbolMaster(javaObj)
  
