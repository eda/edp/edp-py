# EDP imports
from .Electrical import *

class Net(Electrical):
  """Handle to a Net in Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getName(self):
    """Get the name of the net"""
    return str(self._javaObj.getName())
    
  def isGlobal(self):
    """Identify if net is global"""
    return self._javaObj.isGlobal()
    
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.datastruct.Net as JNet

    if JNet.isInstanceOf(javaObj) :
      return Net(javaObj)
    else:
      return []
