from . import DatabaseStructural
from . import DatabaseInstance
from . import DatabaseNetlist
from . import DatabaseCell

class DatabaseNetlist(DatabaseStructural):
  """
  Reference of a netlist in the Expert Design Plan
  """
  
  def addNetlist(self, name:str, master:DatabaseNetlist, connections:[list[str],dict]) -> DatabaseInstance:
    """Instantiate a DatabaseNetlist in this netlist

    Parameters
    ----------
    name : str
      Instance name
    master : DatabaseNetlist
      Netlist that is instantiated 
    connections : list or dict
      List of connected nets or dictonary of connections
      
    Examples
    ----------
    List of net names
    indc=tb.addPrimitive('DUT', nl, ['IN', 'OUT', 'gnd!'])
    
    Dictonary of connections  
    indc=tb.addPrimitive('DUT', nl, {'A': 'IN', 'B':'OUT', 'SUB': 'gnd!'})
    """
    
    if  isinstance(connections, dict):
      import java.util.HashMap as JHashMap
      javaConnections = JHashMap()
     
      for key in connections:
        javaConnections.put(str(key), str(connections[key]))
    else:
      javaConnections=connections
    
    return DatabaseInstance.DatabaseInstance.init(self._javaObj.addInstance(name, master._javaObj, javaConnections))
    
  def addPrimitive(self, name:str, libraryName:str, cellName:str, connections:[list[str],dict]) -> DatabaseInstance:
    """Instantiate a Primitive in the netlist

    Parameters
    ----------
    name : str
      Instance name
    libraryName : str
      Library name of the primitive
    cellName : str
      Cell name of the primitiv
    connections : list or dict
      List of connected nets or dictonary of connections
      
    Examples
    ----------
    List of net names
    indc=tb.addPrimitive('VIN', 'analogLib', 'vdc', ['IN', 'gnd!'])
    
    Dictonary of connections  
    indc=tb.addPrimitive('VIN', 'analogLib', 'vdc', {'PLUS': 'IN', 'MINUS':'gnd!'})
    """
    
    if  isinstance(connections, dict):
      import java.util.HashMap as JHashMap
      javaConnections = JHashMap()
     
      for key in connections:
        javaConnections.put(str(key), str(connections[key]))
    else:
      javaConnections=connections
    
    return DatabaseInstance.DatabaseInstance.init(self._javaObj.addPrimitive(name, libraryName, cellName , javaConnections))
    
  def addCell(self, name:str, master:DatabaseCell, connections:[list[str],dict]) -> DatabaseInstance:
    """Instantiate a cell in the netlist

    Parameters
    ----------
    name : str
      Instance name
    master : DatabaseCell
      Cell to be instantiated 
    connections : list or dict
      List of connected nets or dictonary of connections
      
    Examples
    ----------
    List of net names
    indc=tb.addPrimitive('DUT', cell, ['IN', 'OUT', 'gnd!'])
    
    Dictonary of connections  
    indc=tb.addPrimitive('DUT', cell, {'A': 'IN', 'B':'OUT', 'SUB': 'gnd!'})
    """
    
    return DatabaseInstance.DatabaseInstance.init(self._javaObj.addCell(name, master._javaObj, connections))

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)

  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.DatabaseNetlist as JDatabaseNetlist

    if JDatabaseNetlist.isInstanceOf(javaObj) :
      return DatabaseNetlist(javaObj)
