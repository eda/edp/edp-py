# EDP imports
from .ExpertDesignPlanWrapper import *
from . import Test
from . import Instance
from . import Electrical
from . import Net
from . import Analysis
from . import SimulationResults

class Test(ExpertDesignPlanWrapper):
  """A Test ist used for executing analog simulations in an EDP
  
  Utilize the method `createTest` in a Database to create a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getDatabase(self):
    """Get the database where the test is created"""
    return Database.Database._initdb(self._javaObj.getDatabase())
    
  def getInstance(self, name:str):
    """Get path to an hierarchical instance"""
    return Instance.Instance.init(self._javaObj.getInstance(name))
    
  def getModelFileNames(self) -> list[str]:
    """Get the names of the model files as a list"""
    javaArray = self._javaObj.getModelFileNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def getInstanceNames(self) -> list[str]:
    """Get the names of the instances as a list"""
    javaArray = self._javaObj.getInstanceNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def getInstances(self):
    """Get all instances"""
    
    javaArray = self._javaObj.getInstances()
     
    retval = []

    for elem in javaArray:
      retval.append(Instance.init(elem))

    return retval  
    
  def getInstance(self, name:str):
    """Get a sub-instance"""
    return Instance.Instance.init(self._javaObj.getInstance(name))

  def getNetNames(self) -> list[str]:
    """Get the names of the nets as a list"""
    javaArray = self._javaObj.getNetNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def getNets(self):
    """Get all nets"""
    
    javaArray = self._javaObj.getNets()
     
    retval = []

    for elem in javaArray:
      retval.append(Net.Net.init(elem))

    return retval  
    
  def getNet(self, name:str):
    """Get a net by name"""
    return Net.Net.init(self._javaObj.getNet(name))
    
  def save(self, electrical:Electrical) -> bool:
    """Save an electrical"""
    return self._javaObj.save(electrical._javaObj)
    
  def dontSave(self, electrical:Electrical) ->bool:
    """Don't save an electrical"""
    return self._javaObj.dontSave(electrical._javaObj)
    
  def addAnalysis(self, type:str, title:str) -> Analysis:
    """Add an analysis to the test"""
    return Analysis.Analysis.buildAnalysis(self._javaObj.addAnalysis(type, title))
    
  def getAnalyis(self,title:str) -> Analysis:
    """Get an analysis in the test"""
    return Analysis.Analysis.buildAnalysis(self._javaObj.getAnalyis(title))
    
  def getAnalyses(self) -> list[Analysis]:
    """Get all analyses in the test"""
    
    javaArray = self._javaObj.getAnalyses()

    retval = []

    for elem in javaArray:
      retval.append(Analysis.Analysis.buildAnalysis(elem))

    return retval
    
  def removeAnalysis(self,analysis:Analysis) -> bool:
    """Remove an anylsis from the test"""
    return Analysis.Analysis.buildAnalysis(self._javaObj.removeAnalysis(analysis._javaObj))
    
  def simulate(self):
    """Run a simulation"""
    
    javaArray=self._javaObj.simulate()
  
    if len(javaArray)==1:
      return SimulationResults.SimulationResults.init(javaArray[0])
    else:
      retval = []

      for elem in javaArray:
        retval.append(SimulationResults.SimulationResults.init(elem))
        
      return retval

  def getSessions(self):
      return self._javaObj.getSessions()
      
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.test.Test as JTest

    if JTest.isInstanceOf(javaObj) :
      return Test(javaObj)
    else:
      return []
