# EDP imports
from .Electrical import *
from .Net import *

class OperatingPoint(Electrical):
  """Handle to an operating point in Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getName(self):
    """Get the name of the operating point"""
    return str(self._javaObj.getName())

  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.datastruct.OperatingPoint as JOperatingPoint

    if JOperatingPoint.isInstanceOf(javaObj) :
      return OperatingPoint(javaObj)
    else:
      return []
