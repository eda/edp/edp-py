from . import DatabaseView
from . import DatabaseInstance

class DatabaseStructural(DatabaseView.DatabaseView):
  """Reference of a structural representation (schematic or netlist) in the Expert Design Plan
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)

  def getInstanceNames(self) -> list[str]:
    """Get the names of all instances in the structural representation"""
    
    javaArray = self._javaObj.getInstanceNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval

  def getInstances(self):
    """Get all instances in the structural representation"""
    
    javaArray = self._javaObj.getInstances()

    retval = []

    for elem in javaArray:
      retval.append(DatabaseInstance.DatabaseInstance.init(elem))

    return retval

  def hasInstance(self, instanceName:str) -> bool:
    """Check if a given instance is available in the structural representation
    
    Parameters
    ----------
    instanceName : str
      The name of the instance      
      
    Raises
    ------
    ValueError
      When the provided parameter is not a string
    """
    if isinstance(instanceName, str):
      return self._javaObj.hasInstance(instanceName)
    else:
      raise ValueError("'instanceName' is not a string")
      
  def getInstance(self, instanceName:str) -> DatabaseInstance:
    """Get a instance in the structural description
    
    Parameters
    ----------
    instanceName : str
      The name of the instance      
      
    Raises
    ------
    ValueError
      When the provided parameter is not a string
    """
    if isinstance(instanceName, str):
      return DatabaseInstance.DatabaseInstance.init(self._javaObj.getInstance(instanceName))
    else:
      raise ValueError("'instanceName' is not a string")

      
  def getNets(self) -> list[str]:
    '''
    Get all nets in the view as list
    '''
    javaArray = self._javaObj.getNetNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval  

  def convert(self):
    '''
    Convert the structural description in a netlist
    '''
    from . import DatabaseNetlist
    return DatabaseNetlist.DatabaseNetlist.init(self._javaObj.convert())
      

