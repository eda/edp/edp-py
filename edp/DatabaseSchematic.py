from .DatabaseStructural import *
from . import DatabaseCell

class DatabaseSchematic(DatabaseStructural):
  """
  Reference of a schematic in the Expert Design Plan
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)

  def backannotate(self) -> bool:
    '''
    Backannotate the devices to the design environment
    '''
    return self._javaObj.backannotate()

  def save(self):
    '''
    Backannotate the devices to the design environment
    '''
    return self._javaObj.backannotate()

  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.DatabaseSchematic as JDatabaseSchematic

    if JDatabaseSchematic.isInstanceOf(javaObj) :
      return DatabaseSchematic(javaObj)
