from . import Test
from . import Analysis
from . import ResultsDatabase

class SimulationResults():
  """Handle to the simulation results of a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    self._javaObj = javaObj
    
  def getTest(self) -> Test:
    """Get the test that created the results"""
    return Test.Test.init(self._javaObj.getTest())
    
  def __str__(self):
    return str(self._javaObj.toString())
    
  def get(self, analysisName:str):
    """Get the results database for a given analysis"""
    return ResultsDatabase.ResultsDatabase.init(self._javaObj.get(analysisName))
    
  def getResults(self, analysis:Analysis):
    """Get the results database for a given analysis"""
    return ResultsDatabase.ResultsDatabase.init(self._javaObj.getResults(analysis._javaObj))

  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.test.SimulationResults as JSimulationResults

    if JSimulationResults.isInstanceOf(javaObj) :
      return SimulationResults(javaObj)
