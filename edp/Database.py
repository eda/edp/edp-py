# jpype imports
import jpype
import jpype.imports

# Standard library imports
import os
import sys
import atexit

# EDP imports
from .ExpertDesignPlanWrapper import *
from .DatabaseCell import *
from .DatabaseVerilogADescription import *
from .DatabaseSchematic import *
from .DatabaseView import *
from .RectangularSymbolGenerator import *
from .ReferenceSchematicSymbolGenerator import *
from .SchematicGenerator import *
from .MatchingParametersGroup import *
from .DatabaseStructural import *
from .Test import * 
from .ExtractionEngine import * 

def _initJVM() -> bool:
  """Initalize the JVM for EDP
  
  DO NOT CALL THE FUNCTION YOURSELF
  """
  edp_home_dir = os.getenv('ED_EDP_HOME')

  if isinstance(edp_home_dir, str) :

    jar = edp_home_dir + '/core/jar/edp-core.jar'

    if os.path.isfile(jar) :

      cp = jpype.getClassPath();

      if jar not in cp:
        jpype.addClassPath(jar)
        
        if jpype.isJVMStarted() :
          jpype.shutdownJVM()

      if not jpype.isJVMStarted() :
        jpype.startJVM()

      return True
    else:
      print("JAR '" + jar + "' does not point to a file" , file=sys.stderr)
      return False

class Database(ExpertDesignPlanWrapper):
  """Main entry point for an Expert Design Plan.
  
  Utilize the static method `init` to create a new Database.
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    atexit.register(self.quit)

  def quit(self) -> bool:
    """Quit the database."""
    return self._javaObj.quit()
    
  def quit(self) -> int:
    """Get simulation timeout in s"""
    return self._javaObj.getSimTimeout()    

  def getCadenceWorkingDirectory(self)-> str:
    """Get the working directory of the Virtuoso session"""
    return str(self._javaObj.getCadenceWorkingDirectory())

  def getEvalLib(self) -> str:
    """Get the name of the evaluation library in Virtuoso"""
    return str(self._javaObj.getEvalLib())

  def getSimDir(self) -> str:
    """Get the path to the simulation directory"""
    return str(self._javaObj.getSimDir())
    
  def createMatchingGroup(self) -> MatchingParametersGroup:
    """Create an empty MatchingParametersGroup"""
    return MatchingParametersGroup.init(self._javaObj.createMatchingGroup())
    
  def createNetlist(self, pins:[list[str], None]=None):
    """Create an empty DatabaseNetlist"""
    
    from . import DatabaseNetlist
    
    if pins==None:
      return DatabaseNetlist.DatabaseNetlist.init(self._javaObj.createNetlist([]))
    else:
      return DatabaseNetlist.DatabaseNetlist.init(self._javaObj.createNetlist(pins))
      
  def createTest(self, simulator:str, structural:DatabaseStructural) -> Test:
    """Create a new Test"""
    return Test.init(self._javaObj.createTest(simulator, structural._javaObj))

  def getLibraryNames(self) -> list[str]:
    """Get the library names as a list"""
    javaArray = self._javaObj.getLibNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval

  def getCellNames(self, libName:str) -> list[str]:
    """Get the cell names in a given library

    Parameters
    ----------
    libName : str
      The name of the library to be searched
      
    Raises
    ------
    ValueError
      When the provided parameter is not a string
    """

    if isinstance(libName, str):
      javaArray = self._javaObj.getCellNames(libName)

      retval = []

      for elem in javaArray:
        retval.append(str(elem))

      return retval
    else:
      raise ValueError("'libName' is not a string")

  def getCell(self, libraryName:str, cellName:str) -> DatabaseCell:
    """Get a cell in the database

    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell      
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings
    """
    
    if isinstance(libraryName, str) and isinstance(cellName, str):
      return DatabaseCell.DatabaseCell.init(self._javaObj.getCell(libraryName, cellName))
    else:
      raise ValueError("'libraryName' and/or 'cellName' is not a string")

  def isCell(self, libraryName:str, cellName:str) -> bool:
    """Check if a given cell is available in the database
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell      
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str):
      return self._javaObj.isCell(libraryName, cellName)
    else:
      raise ValueError("'libraryName' and/or 'cellName' is not a string")

  def isPrimitive(self, libraryName:str, cellName:str, viewName:str) -> bool:
    """Check if a primitive is available in the database 
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell  
    viewName : str
      The name of the view            
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str) \
      and isinstance(viewName, str):
      return self._javaObj.isPrimitive(libraryName, cellName, viewName)
    else:
      raise ValueError("'libName' and/or 'cellName' and/or 'viewName' is not a string")

  def getView(self, libraryName:str, cellName:str, viewName:str) -> DatabaseView:
    """Get a  view in the database
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell  
    viewName : str
      The name of the view            
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str) \
      and isinstance(viewName, str):
      javaView= self._javaObj.getView(libraryName, cellName, viewName)
      
      import edlab.eda.edp.core.database.DatabaseVerilogADescription as JDatabaseVerilogADescription
      import edlab.eda.edp.core.database.DatabaseSchematic as JDatabaseSchematic
      
      if JDatabaseVerilogADescription.isInstanceOf(javaView) :
        return DatabaseVerilogADescription.init(javaView)
      elif JDatabaseSchematic.isInstanceOf(javaView) :
        return DatabaseSchematic.init(javaView)
      else:
        return []
      
    else:
      raise ValueError("'libraryName' and/or 'cellName' and/or 'viewName' is not a string")

  def isView(self, libraryName:str, cellName:str, viewName:str) -> bool:
    """Check if a view is in the database available
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell  
    viewName : str
      The name of the view            
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str) \
      and isinstance(viewName, str):
      return self._javaObj.isView(libraryName, cellName, viewName)
    else:
      raise ValueError("'libraryName' and/or 'cellName' and/or 'viewName' is not a string")

  def quit(self) -> bool:
    """Quit the database"""
    return str(self._javaObj.quit())
    
   
  def createRectangularSymbolGenerator(self, libraryName:str, cellName:str, 
    viewName:str, width:float, height:float) -> RectangularSymbolGenerator:
    """Create a rectangualr symbol
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell  
    viewName : str
      The name of the view            
    width : float
      Width of the symbol
    height : float
      Height of the symbol
                  
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str) \
      and isinstance(viewName, str):
      
      import java.math.BigDecimal as JBigDecimal
      
      javaObj = self._javaObj.createRectangularSymbolGenerator(libraryName, 
        cellName, viewName, JBigDecimal(width), JBigDecimal(height))
      
      return RectangularSymbolGenerator.init(javaObj)
      
    else:
      raise ValueError("'libraryName' and/or 'cellName' and/or 'viewName' is not a string")
      
      
  def createSchematicGenerator(self, libraryName:str, cellName:str, viewName:str, interactive:bool=False) -> SchematicGenerator:
    """Create a schematic generator
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell  
    viewName : str
      The name of the view            
    interactive : bool
      Interactive command execution
                  
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str) \
      and isinstance(viewName, str):
      
      javaObj = self._javaObj.createSchematicGenerator(libraryName, 
        cellName, viewName, interactive)
      
      return SchematicGenerator.init(javaObj)
      
    else:
      raise ValueError("'libraryName' and/or 'cellName' and/or 'viewName' is not a string")
      

  def createReferenceSchematicSymbolGenerator(self, libNameSource:str, 
    cellNameSource:str, viewNameSource:str, libNameSymbol:str, 
    cellNameSymbol:str, viewNameSymbol:str) -> ReferenceSchematicSymbolGenerator:
    """Create a reference schematic symbol generator
    
    Parameters
    ----------
    libNameSource : str
      The name of the library of the schematic
    cellNameSource : str
      The name of the cell of the schematic
    viewNameSource : str
      The name of the view  of the schematic    
    libNameSymbol : str
      The name of the library of the symbol
    cellNameSymbol : str
      The name of the cell of the symbol
    viewNameSymbol : str
      The name of the view  of the symbol          
    """

    javaObj = self._javaObj.createReferenceSchematicSymbolGenerator(libNameSource, 
      cellNameSource, viewNameSource, libNameSymbol, cellNameSymbol, viewNameSymbol)
      
    return ReferenceSchematicSymbolGenerator.init(javaObj)
    
  def getExtractionEngine(self, libraryName:str, cellName:str):
    """Get a PREDICT Database

    Parameters
    ----------
    libName : str
      Library name of the device
    cellName : str
      Cell name of the device     
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings
    """
    
    if isinstance(libraryName, str) and isinstance(cellName, str):
      return ExtractionEngine.init(self._javaObj.getExtractionEngine(libraryName, cellName))
    else:
      raise ValueError("'libraryName' and/or 'cellName' is not a string")
      

  @staticmethod
  def quitAll() -> bool:
    """Quit all databases in the JVM"""
    if _initJVM():
      import edlab.eda.edp.core.database.Database as JDatabase
      return JDatabase.quitAll()
      
  @staticmethod
  def isRunningDatabaseAvailable() -> bool:
    """Check if running database is available"""
    import edlab.eda.edp.core.database.Database as JDatabase
    return JDatabase.isRunningDatabaseAvailable()
      
  @staticmethod
  def init(*args):
    """Initialize a new Database
    
    Parameters
    ----------
    *args : 
      Variable length argument list
      When no parameter is provided, it will use '.edpinit' in the working directory.
      Provide a single string parameter to 
    """

    if _initJVM():
      import edlab.eda.edp.core.database.Database as JDatabase

      javaDatabase = []

      if len(args) == 0:
       javaDatabase = JDatabase.init()
      elif len(args) == 1 and isinstance(args[0], str):
       javaDatabase = JDatabase.init(args[0])
      
      if javaDatabase==None:
        raise RuntimeError("Unable to establish a database")
      else:
        return Database._initdb(javaDatabase)
       

  @staticmethod
  def _initdb(javaObj):
    """DO NOT CALL THE FUNCTION YOURSELF"""
    import edlab.eda.edp.core.database.Database as JDatabase

    if JDatabase.isInstanceOf(javaObj) :
      return Database(javaObj)
    else:
      return []
      
  @staticmethod
  def getRunningDatabase():
    """"Get a running database"""
    import edlab.eda.edp.core.database.Database as JDatabase
    return Database._initdb(JDatabase.getRunningDatabase())
