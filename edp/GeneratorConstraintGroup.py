from .ExpertDesignPlanWrapper import *
from . import Database
from . import GeneratedInstance
from . import SchematicGenerator

class GeneratorConstraintGroup(ExpertDesignPlanWrapper):
  """Handle to a constraint group"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def addInstance(self, inst:GeneratedInstance) -> bool:
    """Add an instance to the constraint group"""
    return  self._javaObj.addInstance(inst._javaObj)
      
  def addParameter(self, parameter:str) -> bool:
    """Add a parameter to the group"""
    return  self._javaObj.addParameter(parameter)
      
  def getGenerator(self) -> SchematicGenerator:
    '''
    Get the generator where the constraint group is created
    '''
    return SchematicGenerator.SchematicGenerator.init(self._javaObj.getGenerator())
    
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.schgen.GeneratorConstraintGroup as JGeneratorConstraintGroup

    if JGeneratorConstraintGroup.isInstanceOf(javaObj) :
      return GeneratorConstraintGroup(javaObj)
