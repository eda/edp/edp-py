from .ExpertDesignPlanWrapper import *
from . import Database
from . import SchematicGenerator
from . import GeneratedWire
from . import SymbolMaster

class GeneratedInstance(ExpertDesignPlanWrapper):
  """Handle to an Instance in a SchematicGenerator"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getGenerator(self) -> SchematicGenerator:
    '''
    Get the generator where the wire is created
    '''
    return SchematicGenerator.SchematicGenerator.init(self._javaObj.getGenerator())
    
  def getLibraryName(self) -> str:
    '''
    Get the library name of the master
    '''
    return str(self._javaObj.getLibraryName())

  def getCellName(self) -> str:
    '''
    Get the cell name of the master
    '''
    return str(self._javaObj.getCellName())
    
  def getViewName(self) -> str:
    '''
    Get the view name of the master
    '''
    return str(self._javaObj.getViewName())
    
  def getName(self) -> str:
    '''
    Get the name of the instance
    '''
    return str(self._javaObj.getName()) 
    
  def getSymbolMaster(self) -> SymbolMaster:
    '''
    Get the master of the instance
    '''
    return SymbolMaster.SymbolMaster.init(self._javaObj.getMaster())
    
  def getRotation(self) -> int:
    '''
    Get the rotation of the instance
    '''
    return self._javaObj.getRotation()
    
  def isMirrorX(self) -> bool:
    '''
    Identify if the instance is flipped wrt. to the x axis
    '''
    return self._javaObj.isMirrorX()
    
  def isMirrorY(self) -> bool:
    '''
    Identify if the instance is flipped wrt. to the y axis
    '''
    return self._javaObj.isMirrorY()
    
  def getPosition(self) -> list[float]:
    """Get the position of the instance"""
    point = self._javaObj.getPosition()
    
    if point is None:
      return []
    else:
      return [point.getX().doubleValue(), point.getY().doubleValue()]
      
  def isAnchor(self, name:str) -> bool:
    """Check if an anchor (terminal) with a given name is available"""
    return self._javaObj.isAnchor(name)
    
  def getAnchor(self, anchor:str) -> list[float]:
    """Get the position of an anchor"""
    point = self._javaObj.getAnchor(anchor)
    
    if point is None:
      return []
    else:
      return [point.getX().doubleValue(), point.getY().doubleValue()]
      
  def setProperty(self, name:str, value:[float,int, bool,str]) -> bool:
    """Set a proeprty of the instance"""
    return self._javaObj.setProperty(name, value)
 
  def getAnchors(self) -> list[str]:
    """Get the anchors of the instance as list"""
    javaArray = self._javaObj.getAnchors()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def wire( self
          , anchor:str
          , point:list[float]
          , inc:bool=True
          , name:[str,None]=None
          , ratio:float=1.0
          , route:str='--'
          ) -> GeneratedWire:
    """Draw a wire starting from an instance (anchor)
    
    Parameters
    ----------
    anchor : str
      Anchor of the instance where the wire starts
    point : list[float]
      Second point of the wire
    inc : bool
      Incremental routing
    name : str
      Label that is placed on the wire
    ratio : float
      Draw the route only to a certain percentage, i.e., 0.5 means that only 
      half of the route between anchor and point is drawn
    route : str
      Can be either 
       '--',   : direct route between anchor and point
       '-|'    : draw a perpendicular route from the anchor
                 to the point, starting with a horizontal
                 wire, followed by a vertical wire
       '|-'    : draw a perpendicular route from the anchor
                 to the point, starting with a vertical
                 wire, followed by a horizontal wire
       '-'     : draws a horizontal route starting at the 
                 anchor to the intersection of a horizontal
                 line crossing the anchor and a vertical line
                 crossing the point
       '|'     : draws a vertical route starting at the 
                 anchor to the intersection of a vertical
                 line crossing the anchor and a vertical line
                 crossing the point
    """
    
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint
    
    if route=='-|':
      wire= self._javaObj.drawHorizontalVertical(anchor, 
        JPoint(JBigDecimal(point[0]), JBigDecimal(point[1])), 
        JBigDecimal(ratio), name, inc, 
        False, False, True)
    elif route=='|-':
      wire= self._javaObj.drawVerticalHorizontal(anchor, 
        JPoint(JBigDecimal(point[0]), JBigDecimal(point[1])), 
        JBigDecimal(ratio), name, inc, 
        False, False, True)
    elif route=='-':
      wire= self._javaObj.drawHorizontal(anchor, 
        JPoint(JBigDecimal(point[0]), JBigDecimal(point[1])), 
        JBigDecimal(ratio), name, inc, 
        False, False, True)    
    elif route=='|':
      wire= self._javaObj.drawVertical(anchor, 
        JPoint(JBigDecimal(point[0]), JBigDecimal(point[1])), 
        JBigDecimal(ratio), name, inc, 
        False, False, True)   
    else:
      wire= self._javaObj.draw(anchor, 
        JPoint(JBigDecimal(point[0]), JBigDecimal(point[1])), 
        JBigDecimal(ratio), name, inc, 
        False, False, True)
    
    return GeneratedWire.GeneratedWire.init(wire)
    
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.schgen.GeneratedInstance as JGeneratedInstance

    if JGeneratedInstance.isInstanceOf(javaObj) :
      return GeneratedInstance(javaObj)
