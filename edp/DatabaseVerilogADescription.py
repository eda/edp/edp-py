from .DatabaseView import *

class DatabaseVerilogADescription(DatabaseView):
  """Reference of a VerilogA description in the Expert Design Plan
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getCode(self)-> str:
    """Get the VerilogA code"""
    return str(self._javaObj.getCode())
  
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.DatabaseVerilogADescription as JDatabaseVerilogADescription

    if JDatabaseVerilogADescription.isInstanceOf(javaObj) :
      return DatabaseVerilogADescription(javaObj)
    else:
      return []
