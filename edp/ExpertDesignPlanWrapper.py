class ExpertDesignPlanWrapper:
  """All classes in the Expert Design Toolbox are derived from this class
  """

  def __init__(self, javaObj):
    self._javaObj = javaObj
        
  def __str__(self):
    return str(self._javaObj.toString())

  def __eq__(self, other):
    if isinstance(other, ExpertDesignPlanWrapper):
      return self._javaObj == other._javaObj
    return False
    
  def isCorrupted(self):
    return self._javaObj.isCorrupted()
