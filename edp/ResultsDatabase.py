from . import Electrical
from . import Wave

class ResultsDatabase():
  """Results database of a particular anyalysis
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    self._javaObj = javaObj
    
  def __str__(self):
    return str(self._javaObj.toString())
    
  def get(self, ref:[Electrical, str]):
    """Get a waveform or value from the database"""
    
    if isinstance(ref, Electrical.Electrical):
      javaData = self._javaObj.getByReferenceableElectrical(ref._javaObj)
    else:
      javaData = self._javaObj.getByName(ref)
      
    import edlab.eda.ardb.RealValue as JRealValue
    import edlab.eda.ardb.ComplexValue as JComplexValue
    import edlab.eda.ardb.Waveform as JWaveform
    
    if JRealValue.isInstanceOf(javaData):
      return float(javaData.getValue())
    elif JComplexValue.isInstanceOf(javaData):
      return complex(javaData.getValue().getReal(),
        javaData.getValue().getImaginary())
    elif JWaveform.isInstanceOf(javaData):
      return Wave.Wave.init(javaData)
    else:
      return []
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.ardb.ResultsDatabase as JResultsDatabase

    if JResultsDatabase.isInstanceOf(javaObj) :
      return ResultsDatabase(javaObj)
