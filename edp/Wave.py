from . import Electrical

class Wave():
  """Waveform"""

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    self._javaObj = javaObj
    
  def __str__(self):
    return str(self._javaObj.toString())
    
  def getX(self) -> list[float]:
    """Get the X values of the waveform"""
    javaArray = self._javaObj.getX()

    retval = []

    for elem in javaArray:
      retval.append(float(elem))
    
    return retval

  def getY(self):
    """Get the Y values of the waveform"""
    javaArray = self._javaObj.getY()
    
    import edlab.eda.ardb.RealWaveform as JRealWaveform
    import edlab.eda.ardb.ComplexWaveform as JComplexWaveform
    
    retval = []
    
    if JRealWaveform.isInstanceOf(self._javaObj):
      for elem in javaArray:
        retval.append(float(elem))
    else:
      for elem in javaArray:
        retval.append(complex(elem.getReal(), elem.getImaginary()))
        
    return retval
    
  def getUnitX(self):
    """Get the unit of the x axis"""
    return str(self._javaObj.getUnitX())
    
  def getUnitY(self):
    """Get the unit of the y axis"""
    return str(self._javaObj.getUnitY()) 
    
  def getName(self):
    """Get the name of the waveform"""
    return str(self._javaObj.getName()) 
       
  def abs(self):
    """Get the absolute value of the waveform"""
    return Wave.init(self._javaObj.abs())
    
  def conjugate(self):
    """Calculate the complex conjugate of a waveform"""
    return Wave.init(self._javaObj.conjugate()) 
    
  def real(self):
    """Get the real part of the waveform"""
    return Wave.init(self._javaObj.real())
    
  def imag(self):
    """Get the imag part of the waveform"""
    return Wave.init(self._javaObj.imag()) 
    
  def  phaseDeg(self):
    """Get the phase as degree of the wave """
    return Wave.init(self._javaObj.phaseDeg()) 
    
  def xmin(self):
    """Get the minimal x-value"""
    return float(self._javaObj.xmin())

  def xmax(self):
    """Get the maximal x-value"""
    return float(self._javaObj.xmax())
    
  def clip(self, left:float, right:float):
    """Clip a waveform to a range"""
    return Wave.init(self._javaObj.clip(left, right)) 
    
  def getValue(self, pos:float):
    """Evaluate a waveform at a position"""
    
    javaData = self._javaObj.getValue(pos)
      
    import edlab.eda.ardb.RealValue as JRealValue
    import edlab.eda.ardb.ComplexValue as JComplexValue
    
    if JRealValue.isInstanceOf(javaData):
      return float(javaData.getValue())
    elif JComplexValue.isInstanceOf(javaData):
      return complex(javaData.getValue().getReal(),
        javaData.getValue().getImaginary())
    else:
      return []
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.ardb.Waveform as JWaveform

    if JWaveform.isInstanceOf(javaObj) :
      return Wave(javaObj)
