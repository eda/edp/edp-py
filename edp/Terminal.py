# EDP imports
from .Electrical import *
from .Net import *

class Terminal(Electrical):
  """Handle to a Terminal in Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getName(self):
    """Get the name of the terminal"""
    return str(self._javaObj.getName())

  def getNet(self):
    """Get the net that is associated with the terminal"""
    return Net.init(self._javaObj.getNet())
      
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.datastruct.Terminal as JTerminal

    if JTerminal.isInstanceOf(javaObj) :
      return Terminal(javaObj)
    else:
      return []
