# EDP imports
from . import SweepAnalysis
from . import DatabaseParameter

class DCParameterSweepAnalysis(SweepAnalysis.SweepAnalysis):
  """Handle to a DC Parameter Sweep in a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def setParameter(self, parameter:DatabaseParameter) -> DatabaseParameter:
    """Set the parameter to be swept"""
    return DatabaseParameter.DatabaseParameter.init(self._javaObj.setParameter(parameter._javaObj))

  def getParameter(self) -> DatabaseParameter:
    """Get the parameter that is swept"""
    return DatabaseParameter.DatabaseParameter.init(self._javaObj.getParameter())
    
  def getMaxiters(self) -> int:
    """Get the maxiters"""
    return int(self._javaObj.getMaxiters())
    
  def setMaxiters(self,maxiters:int) -> int:
    """Set the maxiters"""
    return int(self._javaObj.setMaxiters(maxiters))

  def getMaxsteps(self) -> int:
    """Get the maxteps"""
    return int(self._javaObj.getMaxsteps())
    
  def setMaxsteps(self,maxsteps:int) -> int:
    """Get the maxteps"""
    return int(self._javaObj.setMaxsteps(maxsteps))

  def getHomotopy(self) -> str:
    """Get the homotopy"""
    return str(self._javaObj.getHomotopy())
    
  def setHomotopy(self,homotopy:str) -> str:
    """Set the homotopy"""
    return str(self._javaObj.setHomotopy(homotopy))
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.analysis.DCParameterSweepAnalysis as JDCParameterSweepAnalysis

    if JDCParameterSweepAnalysis.isInstanceOf(javaObj) :
      return DCParameterSweepAnalysis(javaObj)
