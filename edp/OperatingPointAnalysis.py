# EDP imports
from .Analysis import *

class OperatingPointAnalysis(Analysis):
  """Handle to an operating point analysis in Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getMaxiters(self) -> int:
    """Get the maxiters"""
    return int(self._javaObj.getMaxiters())
    
  def setMaxiters(self,maxiters:int) -> int:
    """Set the maxiters"""
    return int(self._javaObj.setMaxiters(maxiters))

  def getMaxsteps(self) -> int:
    """Get the maxteps"""
    return int(self._javaObj.getMaxsteps())
    
  def setMaxsteps(self,maxsteps:int) -> int:
    """Get the maxteps"""
    return int(self._javaObj.setMaxsteps(maxsteps))

  def getHomotopy(self) -> str:
    """Get the homotopy"""
    return str(self._javaObj.getHomotopy())
    
  def setHomotopy(self,homotopy:str) -> str:
    """Set the homotopy"""
    return str(self._javaObj.setHomotopy(homotopy))
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.analysis.OperatingPointAnalysis as JOperatingPointAnalysis

    if JOperatingPointAnalysis.isInstanceOf(javaObj) :
      return OperatingPointAnalysis(javaObj)
