# EDP imports
from .ExpertDesignPlanWrapper import *
from . import Instance
from . import Test
from . import DatabaseInstance
from . import Net
from . import Terminal
from . import OperatingPoint

class Instance(ExpertDesignPlanWrapper):
  """Handle to an hierarchical instance in a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getInstanceName(self):
    """Get the name of the instance"""
    return str(self._javaObj.getInstanceName())
    
  def getParent(self):
    """Get the parent instance"""
    return Instance.init(self._javaObj.getParent())
    
  def getInstance(self, name:str):
    """Get a sub-instance"""
    return Instance.init(self._javaObj.getInstance(name))
    
  def getSubInstances(self):
    """Get all sub-instances"""
    
    javaArray = self._javaObj.getSubInstances()
     
    retval = []

    for elem in javaArray:
      retval.append(Instance.init(elem))

    return retval  
     
  def getNetNames(self) -> list[str]:
    '''
    Get the names of the nets as list
    '''
    javaArray = self._javaObj.getNetNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval 
     
  def getNet(self, name:str):
    """Get a net for a given terminal"""
    return Net.init(self._javaObj.getNet(name))
    
  def getTerminalNames(self) -> list[str]:
    '''
    Get the names of the terminals as list
    '''
    javaArray = self._javaObj.getTerminalNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval  
    
  def getTerminal(self, name:str):
    """Get a terminal of a instance"""
    return Terminal.Terminal.init(self._javaObj.getTerminal(name))
    
  def getOperatingPointNames(self) -> list[str]:
    '''
    Get the names of the operating points as list
    '''
    javaArray = self._javaObj.getOperatingPointNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval 
    
  def getOperatingPoint(self, name:str):
    """Get a operating point of a instance"""
    return OperatingPoint.OperatingPoint.init(self._javaObj.getOperatingPoint(name))

  def getTest(self):
    """Get the test where the instance is defined"""
    return Test.Test.init(self._javaObj.getTest())
    
  def getDatabaseInstance(self):
    """Get the corresponding DatabaseInstance"""
    return DatabaseInstance.DatabaseInstance.init(self._javaObj.getDatabaseInstance())
    
  def isTopInst(self) -> bool:
    """Check if is top instance"""
    return self._javaObj.isTopInst()
    
  def get(self, name:str):
    """Get the value of a parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.get(name)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def set(self, name:str, value:[float,int, bool,str]) ->[float,int, bool,str]:
    """Set the value of a parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.set(name, value)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def canSet(self, name:str, value:[float,int, bool,str]) -> bool:
    """Identify if a parameter can be set to a specific value"""
    
    return self._javaObj.canSet(name, value)
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.datastruct.Instance as JInstance

    if JInstance.isInstanceOf(javaObj) :
      return Instance(javaObj)
    else:
      return []
