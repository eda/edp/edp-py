# jpype imports
import jpype

from .ExpertDesignPlanWrapper import *
from . import Database

class RectangularSymbolGenerator(ExpertDesignPlanWrapper):
  """Generator for a rectangular-shaped symbol generator in the Expert Design Plan Toolbox"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getLibraryName(self) -> str:
    '''
    Get the library name of the generator
    '''
    return str(self._javaObj.getLibraryName())

  def getCellName(self) -> str:
    '''
    Get the cell name of the generator
    '''
    return str(self._javaObj.getCellName())
    
  def getViewName(self) -> str:
    '''
    Get the view name of the generator
    '''
    return str(self._javaObj.getViewName())
    
  def getStubLength(self) -> float:
    '''
    Get the stub length
    '''
    return self._javaObj.getStubLength().doubleValue()
    
  def setStubLength(self, stubLength:float) -> float:
    """Set the stub length
    
    Parameters
    ----------
    stubLength : float
      Stub length
    """
    
    import java.math.BigDecimal as JBigDecimal
          
    return self._javaObj.setStubLength(JBigDecimal(stubLength)).doubleValue()
    
  def getWidth(self) -> float:
    '''
    Get the width of the symbol
    '''
    return self._javaObj.getWidth().doubleValue() 
    
  def getHeight(self) -> float:
    '''
    Get the height of the symbol
    '''
    return self._javaObj.getHeight().doubleValue()    
    
  def show(self, show:bool) -> bool:
    """Show the result of the generator in the Virtuoso Symbol editor
    
    Parameters
    ----------
    show : bool
      Show generator result
    """    
    return self._javaObj.show(show)
    
    
  def execute(self) -> bool:
    '''
    Execute the generator
    '''
    return self._javaObj.execute()
    
    
  def addPin( self
            , name:str
            , position:float
            , direction:str='input'
            , edge:str='left'
            , fontHeight:float=0.0625
            , sigType:str='signal'
            , clock:bool=False
            , inverted:bool=False
            ) -> bool:
    """Add a pin to the generator
    
    Parameters
    ----------
    name : str
      Name of the pin
    position : float
      Position   
    direction : str
      Direction, must be 'input', 'output' or 'inputOutput'   
    edge : str
      Edge where the pin is placed, must be 'left', 'right', 'top' or 'bottom'         
    fontHeight : float
      Font height
    sigType : str
      Signal type, must be 'signal', 'ground', 'supply', 'clock', 
      'analog', 'tieOff', 'tieLo', 'scan' and 'reset'
    clock : bool
      Add a clock nose to the pin  
    inverted : bool
      Add an inversion circle to the pin     
    """  

    import java.math.BigDecimal as JBigDecimal
    
    return self._javaObj.addPin(name, direction, edge, JBigDecimal(position),
      JBigDecimal(fontHeight), sigType, clock, inverted)
      
      
      
  def addLabel( self
              , position:list[float]
              , content:str
              , choice:str='pin name'
              , anchor:str='center'
              , rotation:int=0
              , mirrorX:bool=False
              , mirrorY:bool=False
              , type:str='normalLabel'
              , fontHeight:float=0.0625
              ) -> bool:
    """Add a label to the symbol
    
    Parameters
    ----------
    position : list[float]
      Point as two-element list
    content : str
      Content
    anchor : str
      Anchor, must be either 'center', 'northwest', 'north', 'northeast', 
      'east', 'southeast', 'south', 'southwest' or 'southwest'   
    rotation : int
      Rotation, is either 0 (no rotation), 1 (90 degree), 2 (180 degree) 
      or 3 (270 degree)
    mirrorX : bool
      Flip on x axis     
    mirrorY : bool
      Flip on y axis
    type : str
      Type, is either 'normalLabel', 'NLPLabel' or 'ILLabel'
    fontHeight : float
      Font height
    """
    assert len(position) == 2

    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint  
    
    return self._javaObj.addLabel(JPoint(JBigDecimal(position[0]), 
      JBigDecimal(position[1])), choice, content, anchor, rotation, mirrorX, 
      mirrorY, type, JBigDecimal(fontHeight))
      
  def addRectangle(self, ll:list[float], ur:list[float], fill:bool=False) -> bool:
    """Add a rectangle to the symbol
    
    Parameters
    ----------
    ll : list[float]
      Lower left point as two-element list
    ur : list[float]
      Upper right point as two-element list       
    fill : bool
      Fill rectangle
    """
    assert len(ll) == 2
    assert len(ur) == 2
    
    import java.math.BigDecimal as JBigDecimal
    
    return self._javaObj.addRectangle(JBigDecimal(ll[0]), JBigDecimal(ll[1]),
      JBigDecimal(ur[0]), JBigDecimal(ur[1]),fill)
      
  def addLine(self, points:list[list[float]]) -> bool:
    """Add a line to the symbol
    
    Parameters
    ----------
    points: list[list[float]]
      list of points
    """
    
    import edlab.eda.goethe.Point as JPoint  
    import java.math.BigDecimal as JBigDecimal
    import java.util.ArrayList as JArrayList
        
    lst = JArrayList()
    
    for elem in points:
      assert len(elem) == 2
      lst.add(JPoint(JBigDecimal(elem[0]),JBigDecimal(elem[1])))

    return self._javaObj.addLine(lst)
    
    
  def addPolygon(self, points:list[list[float]], fill:bool=False) -> bool:
    """Add a polygon to the symbol
    
    Parameters
    ----------
    points: list[list[float]]
      list of points
    fill : bool
      Fill polygon
    """
    
    import edlab.eda.goethe.Point as JPoint  
    import java.math.BigDecimal as JBigDecimal
    import java.util.ArrayList as JArrayList
        
    lst = JArrayList()
    
    for elem in points:
      assert len(elem) == 2
      lst.add(JPoint(JBigDecimal(elem[0]),JBigDecimal(elem[1])))

    return self._javaObj.addPolygon(lst, fill)
    
  def addEllipse(self, center:list[float], width:float, height:float, fill:bool=False) -> bool:
    """Add an ellipse to the symbol
    
    Parameters
    ----------
    center : list[float]
      Center of the ellipse
    width : float
      Width of the ellipse
    height : float
      Height of the ellipse           
    fill : bool
      Fill ellipse
    """
    assert len(center) == 2
    
    import java.math.BigDecimal as JBigDecimal
    
    return self._javaObj.addEllipse(JBigDecimal(center[0]), 
      JBigDecimal(center[1]), JBigDecimal(width), JBigDecimal(height), fill)

  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.symgen.rect.RectangularSymbolGenerator as JRectangularSymbolGenerator

    if JRectangularSymbolGenerator.isInstanceOf(javaObj) :
      return RectangularSymbolGenerator(javaObj)
