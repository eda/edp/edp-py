from .ExpertDesignPlanWrapper import *
from . import Database
from . import SchematicGenerator
from . import GeneratedPin
from . import GeneratedInstance
from . import SymbolMaster

class GeneratedWire(ExpertDesignPlanWrapper):
  """Handle to a Wire in a SchematicGenerator"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getGenerator(self) -> SchematicGenerator:
    '''
    Get the generator where the wire is created
    '''
    return SchematicGenerator.SchematicGenerator.init(self._javaObj.getGenerator())
     
  def last(self) -> list[float]:
    """Get the last point of the wire"""
    point = self._javaObj.last()
    
    if point is None:
      return []
    else:
      return [point.getX().doubleValue(), point.getY().doubleValue()]
      
  def getWidth(self) -> float:
    """Get the width of the wire"""
    
    return self._javaObj.getWidth().doubleValue()
    
  def setWidth(self, width:float) -> float:
    """Set the width of the wire"""
    import java.math.BigDecimal as JBigDecimal
    return self._javaObj.setWidth(JBigDecimal(width)).doubleValue()
    
  def pin( self
         , name:str
         , rotation:int=0
         , mirrorX:bool=False
         , mirrorY:bool=False
         , direction:str='input' 
         )  -> GeneratedPin:
    """Add a pin to the generator
    
    Parameters
    ----------
    name : str
      Name of the pin
    rotation : int
      Rotation, is either 0 (no rotation), 1 (90 degree), 2 (180 degree) 
      or 3 (270 degree)
    mirrorX : bool
      Flip on x axis     
    mirrorY : bool
      Flip on y axis
    direction : str
      Direction, is either 'input', 'output' or 'inoutOutput'
    """
    
    return GeneratedPin.GeneratedPin.init(self._javaObj.addPin(name, rotation, 
      mirrorX,  mirrorY, direction))
      
      
  def wire( self
          , point:list[float]
          , inc:bool=True
          , name:[str,None]=None
          , ratio:float=1.0
          , route:str='--'
          ) -> 'GeneratedWire':
    """Draw a wire starting at the last point of a wire
    
    Parameters
    ----------
    point : list[float]
      Second point of the wire
    inc : bool
      Incremental routing
    name : str
      Label on wire
    ratio : float
      Draw the route only to a certain percentage, i.e., 0.5 means that only 
      half of the route between anchor and point is drawn
    route : str
      Can be either 
       '--',   : direct route between anchor and point
       '-|'    : draw a perpendicular route from the anchor
                 to the point, starting with a horizontal
                 wire, followed by a vertical wire
       '|-'    : draw a perpendicular route from the anchor
                 to the point, starting with a vertical
                 wire, followed by a horizontal wire
       '-'     : draws a horizontal route starting at the 
                 anchor to the intersection of a horizontal
                 line crossing the anchor and a vertical line
                 crossing the point
       '|'     : draws a vertical route starting at the 
                 anchor to the intersection of a vertical
                 line crossing the anchor and a vertical line
                 crossing the point
    """
    
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint
    
    if route=='-|':
      wire= self._javaObj.drawHorizontalVertical(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), name, inc, 
        False, False, True)
    elif route=='|-':
      wire= self._javaObj.drawVerticalHorizontal(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), name, inc, 
        False, False, True)
    elif route=='-':
      wire= self._javaObj.drawHorizontal(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), name, inc, 
        False, False, True)        
    elif route=='|':
      wire= self._javaObj.drawVertical(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), name, inc, 
        False, False, True)        
    else:
      wire= self._javaObj.draw(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), name, inc, 
        False, False, True)
    
    return GeneratedWire.init(wire)
    
    
  def inst( self
          , master:SymbolMaster
          , anchor:str
          , name:[str,None]=None
          , rotation:int=0
          , mirrorX:bool=False
          , mirrorY:bool=False
          ) -> 'GeneratedInstance':
    """Draw an instance at the last point of the wire
    
    Parameters
    ----------
    master : SymbolMaster
      Master of the symbol to be placed
    anchor : str
      Anchor (terminal) of the symbol to be placed
    name : str
      Name of the instance
    rotation : int
      Rotation, is either 0 (no rotation), 1 (90 degree), 2 (180 degree) 
      or 3 (270 degree)
    mirrorX : bool
      Flip on x axis     
    mirrorY : bool
      Flip on y axis
    """
    return GeneratedInstance.GeneratedInstance.init(
      self._javaObj.addInst(master._javaObj,name,rotation, 
      mirrorX, mirrorY, anchor))
      
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.schgen.GeneratedWire as JGeneratedWire

    if JGeneratedWire.isInstanceOf(javaObj) :
      return GeneratedWire(javaObj)
  
