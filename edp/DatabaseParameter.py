from .ExpertDesignPlanWrapper import *
from . import DatabaseInstance
from . import MatchingParametersGroup

class DatabaseParameter(ExpertDesignPlanWrapper):
  """Reference of a Database Parameter in the Expert Design Plan"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getName(self) -> str:
    '''
    Get the name of the parameter
    '''
    return str(self._javaObj.getName())
    
  def getInstance(self) -> str:
    '''
    Get the associated instance
    '''
    return DatabaseInstance.DatabaseInstance.init(self._javaObj.getInstance())
    
  def getValue(self):
    """Get the value of the parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.getValue()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def set(self, value:[float,int, bool,str]) ->[float,int, bool,str]:
    """Set the value of the parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.set(value)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def canSet(self, value:[float,int, bool,str]) -> bool:
    """Identify if the parametr can be set to a specific value"""
    
    return self._javaObj.canSet(value)
    
  def getMin(self) -> [float,None]:
    """Get lower bound of parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.getMin()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return None
      
  def getMax(self) -> [float,None]:
    """Get upper bound of parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.getMax()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return None
      
  def getGrid(self) -> [float,None]:
    """Get grid of parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.getGrid()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return None
      
  def getInit(self) -> [float,None]:
    """Get init value of parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.getInit()
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return None
      
  def getMatchingGroups(self) -> list[MatchingParametersGroup]:
    '''
    Get all associated matching groups
    '''
    
    javaArray = self._javaObj.getGroupsAsArray()

    retval = []

    for elem in javaArray:
      retval.append(MatchingParametersGroup.MatchingParametersGroup.init(elem))

    return retval
      
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.DatabaseParameter as JDatabaseParameter

    if JDatabaseParameter.isInstanceOf(javaObj) :
      return DatabaseParameter(javaObj)
