from .ExpertDesignPlanWrapper import *
from . import Database
from . import DatabaseView
from . import DatabaseStructural
from . import DatabaseSchematic
from . import DatabaseVerilogADescription

class DatabaseCell(ExpertDesignPlanWrapper):
  """Reference of a Cell in the Expert Design Plan"""

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)

  def getLibraryName(self) -> str:
    '''
    Get the name of the library
    '''
    return str(self._javaObj.getLibraryName())

  def getCellName(self) -> str:
    '''
    Get the name of the cell
    '''
    return str(self._javaObj.getCellName())

  def getDatabase(self)  -> Database:
    """Get the database where the cell is stored"""
    
    return Database.Database._initdb(self._javaObj.getDatabase())

  def getPins(self) -> list[str]:
    '''
    Get the names of the pins as a list
    '''
    javaArray = self._javaObj.getPins()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def getParameters(self) -> list[str]:
    '''
    Get the parameter names as list
    '''
    javaArray = self._javaObj.getParameterNamesAsArray()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval 
    
  def getSwitchList(self) -> list[str]:
    '''
    Get the switch list of the cell as a list
    '''
    javaArray = self._javaObj.getSwitchList()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval 
    
  def setSwitchList(self, switchList:list[str]) -> list[str]:
    """Set the switch list of the cell
    
    Parameters
    ----------
    switchList : list[str]
      List of view names    
    """
    return self._javaObj.setSwitchList(switchList)
    
       
  def getViewNames(self) -> list[str]:
    '''
    Get all view names in the cell
    '''
    javaArray = self._javaObj.getViewNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval  
    
  def getView(self, viewName:[None,str]=None) -> DatabaseView:
    """Get a  view in the database
    
    Parameters
    ----------
    viewName : str
      The name of the view (optional)       
      
    Raises
    ------
    ValueError
      When the provided parameter is not a string
    """
    if isinstance(viewName, str):
      javaView= self._javaObj.getView(viewName)
    else:
      javaView= self._javaObj.getView()
    
    import edlab.eda.edp.core.database.DatabaseVerilogADescription as JDatabaseVerilogADescription
    import edlab.eda.edp.core.database.DatabaseSchematic as JDatabaseSchematic
      
    if JDatabaseVerilogADescription.isInstanceOf(javaView) :
      return DatabaseVerilogADescription.DatabaseVerilogADescription.init(javaView)
    elif JDatabaseSchematic.isInstanceOf(javaView) :
      return DatabaseSchematic.DatabaseSchematic.init(javaView)
    else:
      return []
      
  def hasView(self, viewName:str) -> bool:
    """Identify if a view with a given name is available"""
    return self._javaObj.hasView(viewName)
    
  def hasParameters(self) -> bool:
    """Identify if the cell has parameters"""
    return self._javaObj.hasParameters()

  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.DatabaseCell as JDatabaseCell

    if JDatabaseCell.isInstanceOf(javaObj) :
      return DatabaseCell(javaObj)
