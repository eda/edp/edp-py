from .ExpertDesignPlanWrapper import *
from . import Database
from . import SchematicGenerator
from . import GeneratedWire

class GeneratedPin(ExpertDesignPlanWrapper):
  """Handle to a Pin in a SchematicGenerator"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getGenerator(self) -> SchematicGenerator:
    '''
    Get the generator where the pin is created
    '''
    return SchematicGenerator.SchematicGenerator.init(self._javaObj.getGenerator())
        
  def getName(self) -> str:
    '''
    Get the name of the pin
    '''
    return str(self._javaObj.getName())
    
  def getDirection(self) -> str:
    '''
    Get the direction
    '''
    return str(self._javaObj.getDirection())
    
  def getRotation(self) -> int:
    '''
    Get the rotation of the pin
    '''
    return self._javaObj.getRotation()
    
  def isMirrorX(self) -> bool:
    '''
    Identify if the pin is flipped wrt. to the x axis
    '''
    return self._javaObj.isMirrorX()
    
  def isMirrorY(self) -> bool:
    '''
    Identify if the pin is flipped wrt. to the y axis
    '''
    return self._javaObj.isMirrorY()  
     
  def getPosition(self) -> list[float]:
    """Get the position of the pin"""
    point = self._javaObj.getPosition()
    
    if point is None:
      return []
    else:
      return [point.getX().doubleValue(), point.getY().doubleValue()]
      
  def wire( self
          , point:list[float]
          , inc:bool=True
          , ratio:float=1.0
          , route:str='--'
          ) -> GeneratedWire:
    """Draw a wire starting from a pin (anchor)
    
    Parameters
    ----------
    point : list[float]
      Second point of the wire
    inc : bool
      Incremental routing
    ratio : float
      Draw the route only to a certain percentage, i.e., 0.5 means that only 
      half of the route between anchor and point is drawn
    route : str
      Can be either 
       '--',   : direct route between anchor and point
       '-|'    : draw a perpendicular route from the anchor
                 to the point, starting with a horizontal
                 wire, followed by a vertical wire
       '|-'    : draw a perpendicular route from the anchor
                 to the point, starting with a vertical
                 wire, followed by a horizontal wire
       '-'     : draws a horizontal route starting at the 
                 anchor to the intersection of a horizontal
                 line crossing the anchor and a vertical line
                 crossing the point
       '|'     : draws a vertical route starting at the 
                 anchor to the intersection of a vertical
                 line crossing the anchor and a vertical line
                 crossing the point
    """
    
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint
    
    if route=='-|':
      wire= self._javaObj.drawHorizontalVertical(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), inc, 
        False, False, True)
    elif route=='|-':
      wire= self._javaObj.drawVerticalHorizontal(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), inc, 
        False, False, True)
    elif route=='-':
      wire= self._javaObj.drawHorizontal(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), inc, 
        False, False, True)        
    elif route=='|':
      wire= self._javaObj.drawVertical(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), inc, 
        False, False, True)        
    else:
      wire= self._javaObj.draw(JPoint(JBigDecimal(point[0]), 
        JBigDecimal(point[1])), JBigDecimal(ratio), inc, 
        False, False, True)
    
    return GeneratedWire.GeneratedWire.init(wire)
  
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.schgen.GeneratedPin as JGeneratedPin

    if JGeneratedPin.isInstanceOf(javaObj) :
      return GeneratedPin(javaObj)
  
