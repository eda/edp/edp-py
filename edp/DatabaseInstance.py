# EDP imports
from .ExpertDesignPlanWrapper import *
from . import DatabaseStructural 
from . import DatabaseParameter 

class DatabaseInstance(ExpertDesignPlanWrapper):
  """ Reference of a database instance in the Expert Design Plan
  """

  def __init__(self, javaObj):
    super().__init__(javaObj)

  def getMasterLibraryName(self):
    '''
    Get the master library name
    '''
    return str(self._javaObj.getMasterLibraryName())

  def getMasterCellName(self):
    '''
    Get the master cell name
    '''
    return str(self._javaObj.getMasterCellName())

  def getInstanceName(self, bit:[int,None]=None):
    '''
    Get the name of the instance
    '''
    if bit==None:
      return str(self._javaObj.getInstanceName())
    else:
      return str(self._javaObj.getInstanceName(bit))
      
  def getDatabase(self):
    '''
    Get the database where the instance is stored
    '''
    return Database.initdb(self._javaObj.getDatabase())
    
  def getView(self) -> DatabaseStructural:
    """Get the structrual view that contains the instance"""
    javaView= self._javaObj.getView()
    
    import edlab.eda.edp.core.database.DatabaseNetlist as JDatabaseNetlist
    import edlab.eda.edp.core.database.DatabaseSchematic as JDatabaseSchematic
      
    if JDatabaseNetlist.isInstanceOf(javaView) :
      from . import DatabaseNetlist 
      return DatabaseNetlist.DatabaseNetlist.init(javaView)
    elif JDatabaseSchematic.isInstanceOf(javaView) :
      from . import DatabaseSchematic
      return DatabaseSchematic.init(javaView)
    else:
      return []
      
  def descend(self) -> DatabaseStructural:
    """Descend in an instance"""
    javaView= self._javaObj.getView()
    
    import edlab.eda.edp.core.database.DatabaseNetlist as JDatabaseNetlist
    import edlab.eda.edp.core.database.DatabaseSchematic as JDatabaseSchematic
      
    if JDatabaseNetlist.isInstanceOf(javaView) :
      from . import DatabaseNetlist 
      return DatabaseNetlist.DatabaseNetlist.init(javaView)
    elif JDatabaseSchematic.isInstanceOf(javaView) :
      from . import DatabaseSchematic
      return DatabaseSchematic.init(javaView)
    else:
      return []

  def setProperty(self, name:str, value:[float,int, bool,str]) -> bool:
    """Set a property of the instance"""
    return self._javaObj.setProperty(name, value)

  def getProperty(self, name:str):
    """Get a property of the instance"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.getProperty(name)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def hasProperty(self, name:str) -> bool:
    """Identify if a parameter with a given name is available"""
    
    return self._javaObj.hasProperty(name)
    
  def getParameter(self, name:str):
    """Get a parameter handle"""
    
    return DatabaseParameter.DatabaseParameter.init(self._javaObj.getParameter(name))

  def get(self, name:str):
    """Get the value of a parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.get(name)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def set(self, name:str, value:[float,int, bool,str]) ->[float,int, bool,str]:
    """Set the value of a parameter"""
    
    import java.math.BigDecimal as JBigDecimal
    
    javaValue= self._javaObj.set(name, value)
    
    if isinstance(javaValue, JBigDecimal):
      return javaValue.doubleValue()
    else:
      return javaValue
      
  def canSet(self, name:str, value:[float,int, bool,str]) -> bool:
    """Identify if a parameter can be set to a specific value"""
    
    return self._javaObj.canSet(name, value)
    
  def hasTechProperty(self, name:str) -> bool:
    """Identify if a technology property is available"""
    
    return self._javaObj.hasTechProperty(name)
    
  def hasTechProperty(self, name:str, key:str) -> bool:
    """Identify if a technology property is available"""
    
    return self._javaObj.hasTechProperty(name, key)
    
  def getTechProperty(self, name:str) -> float:
    """Get a technology property"""
    
    return self._javaObj.getTechProperty(name).doubleValue()
    
  def getTechProperty(self, name:str, key:str) -> float:
    """Get a technology property"""
    
    return self._javaObj.getTechProperty(name, key).doubleValue() 
    
  def getBaseName(self) -> str:
    """Get the base name"""
    
    return self._javaObj.getBaseName()
    
  def getInstanceNames(self) -> list[str]:
    """Get instance names as a list"""
    javaArray = self._javaObj.getInstanceNames()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def getNumOfBits(self) -> str:
    """Get the number of bits"""
    
    return self._javaObj.getNumOfBits()
    
  def getNet(self, term:str) -> float:
    """Get the net that is connected to a particular terminal"""
    
    return self._javaObj.getNet(term)
    
  def getNets(self, bit:[int,None]=None) -> list[str]:
    """Get the net names that are connected to the terminals of the of the instance"""

    
    if bit==None:
      javaArray = self._javaObj.getNets()
    else:
      javaArray = self._javaObj.getNets(bit)

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval
    
  def getSwitchList(self) -> list[str]:
    '''
    Get the switch list of the instance as a list
    '''
    javaArray = self._javaObj.getSwitchList()

    retval = []

    for elem in javaArray:
      retval.append(str(elem))

    return retval 
    
  def setSwitchList(self, switchList:list[str]) -> list[str]:
    """Set the switch list of the instance
    
    Parameters
    ----------
    switchList : list[str]
      List of view names    
    """
    return self._javaObj.setSwitchList(switchList)
    
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.database.DatabaseInstance as JDatabaseInstance

    if JDatabaseInstance.isInstanceOf(javaObj) :
      return DatabaseInstance(javaObj)
