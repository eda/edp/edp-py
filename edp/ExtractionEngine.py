import numpy as np

class ExtractionEngine():
  """Handle to a PREDICT database"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    self._javaEngine = javaObj
    self._javaEnv = javaObj.getEnvironment()
    
  def __str__(self):
    return str(self._javaEnv.toString())
    
  def getLibraryName(self) -> str:
    """Get the library name"""
    return str(self._javaEnv.getLibraryName())
    
  def getCellName(self) -> str:
    """Get the cell name"""
    return str(self._javaEnv.getCellName())
    
  def getModelName(self) -> str:
    """Get the model name"""
    return str(self._javaEnv.getModelName())
    
  def getModelType(self) -> str:
    """Get the model type"""
    return str(self._javaEnv.getModelType())
      
  def getTag(self) -> str:
    """Get the tag of the database"""
    return str(self._javaEnv.getTag())
    
  def getConstantModels(self) -> list[str]:
    """Get the names of the constant models"""
    
    javaConstantModels = self._javaEnv.getConstantModels()
    
    retval = []

    for elem in javaConstantModels:
      retval.append(str(elem.getName()))
      
    return retval
    
  def getVariableModels(self) -> list[str]:
    """Get the names of the variable models"""
    
    javaVariableModels = self._javaEnv.getVariableModels()
    
    retval = []

    for elem in javaVariableModels:
      retval.append(str(elem.getName()))
      
    return retval
    
  def getSections(self, name:str) -> list[str]:
    """Get the sections of the variable model"""
    
    javaVariableModel = self._javaEnv.getVariableModel(name)
    
    import edlab.eda.predict.model.VariableModel  as JVariableModel
    
    if JVariableModel.isInstanceOf(javaVariableModel):
    
      javaSections = javaVariableModel.getSectionAliases()

      retval = []
      
      for elem in javaSections:
        retval.append(str(elem))
        
      return retval
      
  def getOperatingPoints(self) -> list[str]:
    """Get the names of all operating points in the database"""
    
    javaOperatingPoints = self._javaEnv.getOperatingPoints()
    
    retval = []

    for elem in javaOperatingPoints:
      retval.append(str(elem.getName()))
      
    return retval  
    
  def getOperatingPointUnit(self, name:str) -> str:
    """Get the unit of an operating point with a given name"""
    
    javaOperatingPoint = self._javaEnv.getOperatingPoint(name)
    
    import edlab.eda.predict.model.OperatingPoint  as JOperatingPoint
    
    if JOperatingPoint.isInstanceOf(javaOperatingPoint):
      return str(javaOperatingPoint.getUnit())
      
  def getTemperatures(self) -> list[float]:
    """Get the temperatures that were used during characterization"""
    
    javaRange = self._javaEnv.getTemperatures()
    
    import edlab.eda.predict.model.ParameterRange as JParameterRange
    
    if JParameterRange.isInstanceOf(javaRange):
    
      javaArray = javaRange.getValuesAsArray()
      retval = []

      for elem in javaArray:
        retval.append(float(elem.doubleValue()))
        
      return retval
      
  def getVariableCurrents(self) -> list[str]:
    """Get the names of all variable currents used during characterization"""
    
    javaVariableCurrents = self._javaEnv.getVaryingCurrents()
    
    retval = []

    for elem in javaVariableCurrents:
      retval.append(str(elem.getName()))
      
    return retval  
    
  def isVariableCurrent(self, name:str) -> bool:
    """Identify if a given current is varied in the database"""
    
    return self._javaEnv.isVaryingCurrentsRange(name)
    
  def getVariableCurrentValues(self, name:str) -> list[float]:
    """Get the current values that are varied"""
    
    javaRange = self._javaEnv.getVaryingCurrentsRange(name)
      
    import edlab.eda.predict.model.ElectricalRange as JElectricalRange
    
    if JElectricalRange.isInstanceOf(javaRange):
    
      javaArray = javaRange.getValuesAsArray()
      retval = []

      for elem in javaArray:
        retval.append(float(elem.doubleValue()))
        
      return retval
      
  def getVariableVoltages(self) -> list[str]:
    """Get the names of all variable voltages used during characterization"""
    
    javaVariableVoltages = self._javaEnv.getVaryingVoltages()
    
    retval = []

    for elem in javaVariableVoltages:
      retval.append(str(elem.getName()))
      
    return retval
     
  def isVariableVoltage(self, name:str) -> bool:
    """Identify if a given voltage is varied in the database"""
    
    return self._javaEnv.isVaryingVoltageRange(name)
    
  def getVariableVoltageValues(self, name:str) -> list[float]:
    """Get the voltages values that are varied"""
    
    javaRange = self._javaEnv.getVaryingVoltageRange(name)
      
    import edlab.eda.predict.model.ElectricalRange as JElectricalRange
    
    if JElectricalRange.isInstanceOf(javaRange):
    
      javaArray = javaRange.getValuesAsArray()
      retval = []

      for elem in javaArray:
        retval.append(float(elem.doubleValue()))
        
      return retval 
      
  def getVariableParameters(self) -> list[str]:
    """Get the names of all parameters used during characterization"""
    
    javaVariableParameters = self._javaEnv.getVaryingParameters()
    
    retval = []

    for elem in javaVariableParameters:
      retval.append(str(elem.getName()))
      
    return retval
    
  def isVariableParameter(self, name:str) -> bool:
    """Identify if a given parameter is varied in the database"""
    
    return self._javaEnv.isVaryingParameterRange(name) 
    
  def getVariableParameterValues(self, name:str) -> list[float]:
    """Get the parameter values that are varied"""
    
    javaRange = self._javaEnv.getVaryingParameterRange(name)
      
    import edlab.eda.predict.model.ParameterRange as JParameterRange
    
    if JParameterRange.isInstanceOf(javaRange):
    
      javaArray = javaRange.getValuesAsArray()
      retval = []

      for elem in javaArray:
        retval.append(float(elem.doubleValue()))
        
      return retval
      
  def getDefaultEvalOrder(self) -> list[str]:
    """Get the default evaluation order"""
    
    javaIdentifiersArray = self._javaEngine.getIdentifiersAsArray()
    
    retval = []

    for elem in javaIdentifiersArray:
      retval.append(str(elem.getName()))
      
    return retval
    
  def getPoints(self, **kwargs):
    """Get data from the LUT as nd-array

    Keyword-Parameters
    ----------
    operatingPoints: here, the desired operating points must be provided
      key    : 'operatingPoints'
      value  : list of operating points,e.g. ['gm', 'id', 'vdsat']
    varying voltages:
       key    : cell arraying, starting with 'V_', followed by the terminals, e.g.
               'V_S' or 'V_GS'
       value  : list of values
    varying parameters:
        key    : cell arraying, starting with 'P_', followed by the name, e.g.
                  'P_w' or 'P_l'
        value  : array of values
    evaluation order: order in which the parameters are evaluated
      key    : evalOrder
      value  : array of identifiers  
    """
    if not any(x in kwargs.get('operatingPoints') for x in self.getOperatingPoints()):
      raise ValueError("Provided operating points are not in the database")
  
    javaOperatingPoints=[]
    
    for elem in kwargs.get('operatingPoints'):
      javaOperatingPoints.append(self._javaEnv.getOperatingPoint(elem))
      
    models=kwargs.get('models')
    
    if type(models) is dict:
    
      javaModels=[]
    
      for k, v in models.items():
        
        if not isinstance(v, str):
          raise ValueError("Models is invalid")
        else:
          javaModels.append([k,v])
          
      if len(javaModels)==len(self._javaEnv.getVariableModels()):
         
        modelIndex = self._javaEnv.getModelIndex(javaModels)
        
        if modelIndex < 0:
          raise ValueError('Provided model combination is not available')
          
      else:
        raise ValueError('Provided models do not match')
    else:
      raise ValueError("Provided models is not a dict")
      
    evalOrder=kwargs.get('evalOrder')
    
    if not isinstance(evalOrder, list):
      evalOrder = self.getDefaultEvalOrder()
      
    import collections
    
    if collections.Counter(evalOrder) != collections.Counter(self.getDefaultEvalOrder()):
      raise ValueError("Evaluation order invalid")
      
    javaEvaluationOrder=[]
    
    for elem in evalOrder:
      javaEvaluationOrder.append(self._javaEngine.getIdentifier(elem))
      
    javaEvalRoutine = self._javaEngine.createRoutine(javaEvaluationOrder)
    
    matrixSize=[]

    for identifier in javaEvaluationOrder:  
      values=kwargs.get(str(identifier.getName()))
     
      if isinstance(values, list):
        
        import java.math.BigDecimal as JBigDecimal
        
        javaBigDecimalArray=[]
          
        for elem in values:
          javaBigDecimalArray.append(JBigDecimal(elem))
        
        javaEvalRoutine.setValuesExplicitForIdentifier(identifier, javaBigDecimalArray)
    
      matrixSize.append(javaEvalRoutine.getValues(identifier).getSize())

    
    javaMap = self._javaEngine.extract(modelIndex, javaOperatingPoints, javaEvalRoutine)
    
    retval=dict()
    
    for javaOperatingPoint in javaOperatingPoints:  
      
      javaWave = javaMap.get(javaOperatingPoint.getName()).unwrap().getWave()
      
      values = []

      for elem in javaWave:
        values.append(float(elem.doubleValue()))
      
      retval[javaOperatingPoint.getName()] = np.array(values).reshape(matrixSize)
     
    return retval
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.predict.extraction.ExtractionEngine as JExtractionEngine

    if JExtractionEngine.isInstanceOf(javaObj) :
      return ExtractionEngine(javaObj)

