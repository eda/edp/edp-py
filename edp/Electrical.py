# EDP imports
from .ExpertDesignPlanWrapper import *
from . import Test
from . import Instance

class Electrical(ExpertDesignPlanWrapper):
  """Handle to an electrical property in a Test
  """

  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getTest(self):
    """Get the test where the electrical is defined"""
    return Test.Test.init(self._javaObj.getTest())
    
  def getInstance(self):
    """Get the instance where the electrical is defined"""
    return Instance.Instance.init(self._javaObj.getInstance())
    
  @staticmethod
  def init(javaObj):
    import edlab.eda.edp.simulation.datastruct.Electrical as JElectrical

    if JElectrical.isInstanceOf(javaObj) :
      return Test(javaObj)
    else:
      return []
