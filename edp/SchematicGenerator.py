from .ExpertDesignPlanWrapper import *
from . import Database
from . import SymbolMaster
from . import GeneratedPin
from . import GeneratedWire
from . import GeneratorConstraintGroup
from . import GeneratedInstance


class SchematicGenerator(ExpertDesignPlanWrapper):
  """Generator for a schematic generator in the Expert Design Plan Toolbox"""
  
  def __init__(self, javaObj):
    """Constructor - DO NOT CALL THE CONSTRUCTOR YOURSELF"""
    super().__init__(javaObj)
    
  def getLibraryName(self) -> str:
    '''
    Get the library name of the generator
    '''
    return str(self._javaObj.getLibraryName())

  def getCellName(self) -> str:
    '''
    Get the cell name of the generator
    '''
    return str(self._javaObj.getCellName())
    
  def getViewName(self) -> str:
    '''
    Get the view name of the generator
    '''
    return str(self._javaObj.getViewName())
    
  def getDatabase(self)  -> Database:
    """Get the database where the generator is created"""
    
    return Database.Database._initdb(self._javaObj.getDatabase())

  def addConstraintGroup(self)  -> GeneratorConstraintGroup:
    """Add an empty contraint group to the Generator"""
    
    return GeneratorConstraintGroup.GeneratorConstraintGroup.init(
      self._javaObj.addConstraintGroup())
    
    
  def setShowBoundingBox( self
            , ll:list[float]
            , ur:list[float]
            )  -> list[list[float],list[float]]:
    """Set the show bounding box of the generator
    
    Parameters
    ----------
    ll : list[float]
      Lower left point of the bounding box
    ur : list[float]
      Upper right point of the bounding box
    """
    assert len(ll) == 2
    assert len(ur) == 2
        
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint  
    

    bbox= self._javaObj.setShowBoundingBox(
      JPoint(JBigDecimal(ll[0]), JBigDecimal(ll[1])),
      JPoint(JBigDecimal(ur[0]), JBigDecimal(ur[1])))
      
    return [[bbox.getLowerLeft().getX().doubleValue(),
      bbox.getLowerLeft().getY().doubleValue()],
      [bbox.getUpperRight().getX().doubleValue(),
      bbox.getUpperRight().getY().doubleValue()]]
      
  def getShowBoundingBox(self) -> list[list[float],list[float]]:
    """Get the show bounding box of the generator"""
    
    bbox= self._javaObj.getShowBoundingBox()

    return [[bbox.getLowerLeft().getX().doubleValue(),
      bbox.getLowerLeft().getY().doubleValue()],
      [bbox.getUpperRight().getX().doubleValue(),
      bbox.getUpperRight().getY().doubleValue()]]  
    
  def pin( self
            , position:list[float]
            , name:str
            , rotation:int=0
            , mirrorX:bool=False
            , mirrorY:bool=False
            , direction:str='input' 
            )  -> GeneratedPin:
    """Add a pin to the generator
    
    Parameters
    ----------
    position : list[float]
      Point as two-element list
    name : str
      Name of the pin
    rotation : int
      Rotation, is either 0 (no rotation), 1 (90 degree), 2 (180 degree) 
      or 3 (270 degree)
    mirrorX : bool
      Flip on x axis     
    mirrorY : bool
      Flip on y axis
    direction : str
      Direction, is either 'input', 'output' or 'inoutOutput'
    """
    assert len(position) == 2
    
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint  
    return GeneratedPin.GeneratedPin.init(self._javaObj.addPin(
      JPoint(JBigDecimal(position[0]), JBigDecimal(position[1])),
      name, rotation, mirrorX, mirrorY, direction))
      
  def wire( self
            , start:list[float]
            , end:list[float]
            , name:[str,None]=None
            )  -> GeneratedWire:
    """Add a wire to the generator
    
    Parameters
    ----------
    start : list[float]
      Point where the wire starts
    end : list[float]
      Point where the wire starts
    name : str
      Label on the wire
    """
    assert len(start) == 2
    assert len(end) == 2
        
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint  
    
    return GeneratedWire.GeneratedWire.init(self._javaObj.addWire(
      JPoint(JBigDecimal(start[0]), JBigDecimal(start[1])),
      JPoint(JBigDecimal(end[0]), JBigDecimal(end[1])),
      name))
      
  def inst( self
          , master:SymbolMaster
          , position: list[float]
          , name:[str,None]=None
          , rotation:int=0
          , mirrorX:bool=False
          , mirrorY:bool=False
          ) -> 'GeneratedInstance':
    """Draw an instance 
    
    Parameters
    ----------
    master : SymbolMaster
      Master of the symbol to be placed
    position : list[float]
      Position where the instance is placed
    name : str
      Name of the instance
    rotation : int
      Rotation, is either 0 (no rotation), 1 (90 degree), 2 (180 degree) 
      or 3 (270 degree)
    mirrorX : bool
      Flip on x axis     
    mirrorY : bool
      Flip on y axis
    """
    
    assert len(position) == 2
        
    import java.math.BigDecimal as JBigDecimal
    import edlab.eda.goethe.Point as JPoint  
    
    return GeneratedInstance.GeneratedInstance.init(
      self._javaObj.addInst(master._javaObj, name,
      JPoint(JBigDecimal(position[0]), JBigDecimal(position[1])),
      rotation, mirrorX, mirrorY))
      
    
  def show(self, show:bool) -> bool:
    """Show the result of the generator in the Virtuoso Schematic editor
    
    Parameters
    ----------
    show : bool
      Show generator result
    """    
    return self._javaObj.show(show)
    
  def interactive(self, interactive:bool) -> bool:
    """Set the interactive property of the generator
    
    Parameters
    ----------
    interactive : bool
      Interactive execution of the generator
    """    
    return self._javaObj.interactive(interactive)
    
  def automaticCheck(self, check:bool) -> bool:
    """Set the check property of the generator. 
    
    When this feature is enabled, the tool will check the schematic 
    (extract the connectivity) every time when the
    generator is executed.

    Parameters
    ----------
    check : bool
      Enable automatic check
    """    
    return self._javaObj.automaticCheck(check)    
    
  def check(self) -> bool:
    '''
    Check and Save the schematic
    '''
    return self._javaObj.check()
    
  def execute(self) -> bool:
    '''
    Execute the generator
    '''
    return self._javaObj.execute()

  def getSymbolMaster(self, libraryName:str, cellName:str, viewName:str='symbol') -> SymbolMaster:
    """Get the master of a symbol
    
    Parameters
    ----------
    libName : str
      The name of the library
    cellName : str
      The name of the cell  
    viewName : str
      The name of the view            
      
    Raises
    ------
    ValueError
      When the provided parameters are not a strings 
    """
    if isinstance(libraryName, str) and isinstance(cellName, str) \
      and isinstance(viewName, str): 
      return SymbolMaster.SymbolMaster.init(self._javaObj.getSymbolMaster(libraryName, cellName, viewName))
    else:
      raise ValueError("'libraryName' and/or 'cellName' and/or 'viewName' is not a string")
  
  @staticmethod
  def init(javaObj):

    import edlab.eda.edp.core.schgen.SchematicGenerator as JSchematicGenerator

    if JSchematicGenerator.isInstanceOf(javaObj) :
      return SchematicGenerator(javaObj)
  
