import setuptools

package_name = 'edp'

#with open('README.md', 'r') as fh:
#    long_description = fh.read()

with open('requirements.txt', 'r') as req:
    requirements = req.read().splitlines()

setuptools.setup( name=package_name
                , version='0.0.1' 
                , description='Python Wrapper for the Expert Design Plan (EDP) Toolbox'
                , url='https://gitlab-forschung.reutlingen-university.de/schweikm/edp-py'
                , author='Matthias Schweikardt'
                , author_email='matthias.schweikardt@reutlingen-university.de'
                , packages = [package_name]
                , python_requires= '>=3.0'
                , install_requires = requirements
                , classifiers=[ 'Development Status :: 2 :: Pre-Alpha'
                              , 'Operating System :: POSIX :: Linux'       
                              , 'Programming Language :: Python :: 3.5']
)
